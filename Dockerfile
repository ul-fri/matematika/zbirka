FROM python:3

WORKDIR /root
RUN apt-get clean -qq
RUN apt-get update -qq
RUN apt-get install -f -y -qq tzdata 
RUN apt-get install -f -y --no-install-recommends texlive-latex-extra texlive-extra-utils latexmk texlive-lang-european texlive-fonts-extra
COPY requirements.txt /root
RUN python3 -m pip install -r requirements.txt
RUN apt-get install -f -y -qq --no-install-recommends texlive-fonts-recommended
RUN apt-get autoremove
