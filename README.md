# Zbirka nalog za predmet Matematika na FRI

[![pipeline status](https://gitlab.com/ul-fri/matematika/zbirka/badges/master/pipeline.svg)](https://gitlab.com/ul-fri/matematika/zbirka/commits/master)

## Prevedi zbirko

Če želite prevesti zbirko, preprosto poženite

```
python3 prevedi.py
```

### Docker

Če imate težave z namestitvijo zahtevanih programov, lahko zbirko prevedete tudi z [Docker](https://docker.io) sliko

```
docker run -t -i -v  $(pwd):/root registry.gitlab.com/ul-fri/matematika/zbirka/latex:latest python3 prevedi.py
```

## Priprava okolja

Za prevajanje zbirke potrebujete Latex in Python. V linuxu lahko vse potrebno namestite z naslednjimi ukazi

```
apt-get update -qq && apt-get install -y -qq texlive-latex-extra texlive-extra-utils latexmk texlive-lang-european texlive-fonts-extra python3-pip
```

Nato še Python knjižnice 

```
pip3 install -r requirements
```

## Docker slika za Gitlab CI

ČE želimo zgraditi zbirko z [Gitlab CI](https://docs.gitlab.com/ee/ci/README.html) lahko [uporabimo lastno docker sliko](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html).

Najprej sliko zgradimo na lastnem računalniku

```
docker build -t registry.gitlab.com/ul-fri/latex .
```

### Prenos v Gitlab registry

Sliko naložimo v Gitlabovo [zbirko docker slik](https://registry.gitlab.com) 

```
docker login registry.gitlab.com
docker push registry.gitlab.com/ul-fri/latex:latest
```
