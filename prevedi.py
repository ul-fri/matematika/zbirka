#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import os

poglavja = ['01_enacbe',
    '02_kompleksna',
    '03_zaporedja',
    '04_vrste',
    '05_funkcije',
    '06_odvod',
    '07_integral',
    '08_vektorji',
    '09_geometrija',
    '10_sistemi',
    '11_matrike',]

REZULTAT_TPL = """\\begin{{rezultatvp}}\\label{{{label} O}}
\\hfill\\hyperref[{label} N]{{\\linkn}}
{rezultat}
\\end{{rezultatvp}}
""" 
RESITEV_TPL = """\\begin{{resitevp}}\\label{{{label} R}}
\\hfill\\hyperref[{{{label} N}}]{{\\linkn}}
{resitev}
\\end{{resitevp}}
""" 
CHAPTER_RE = re.compile(r"\\chapter{(.*?)}")
REZULTAT_RE = re.compile(r"^\s*\\begin{rezultat}(.*?)\\end{rezultat}", re.DOTALL| re.M)
RESITEV_RE = re.compile(r"^\s*\\begin{resitev}(.*?)\s*\\end{resitev}", re.DOTALL| re.M)
NALOGA_RE = re.compile(r"^\s*\\begin{naloga}.*?\\end{naloga}", re.DOTALL| re.M)
KOMENTAR_RE = re.compile(r"%.*")
LABEL_RE = re.compile(r"\\begin{naloga}{(.*?)}")

for poglavje in poglavja:
    with open("{0}.tex".format(poglavje), encoding="utf-8") as fp:
        source = fp.read()
    # source = KOMENTAR_RE.sub("", source) To bo uredil tex. Problem je, ker pobere tudi \%
    ime = CHAPTER_RE.findall(source)[0]
    naloge = NALOGA_RE.findall(source)
    with open("{}_resitve.tex".format(poglavje), "w", encoding="utf-8") as fp:
        fp.write("\\section*{{Rezultati: {0}}}\n".format(ime))
        for naloga in naloge:
            label = LABEL_RE.findall(naloga)[0]
            rezultat = REZULTAT_RE.findall(naloga)
            if len(rezultat)>0:
                fp.write(REZULTAT_TPL.format(label=label, rezultat=rezultat[0]))
            else:
                fp.write("\\addtocounter{rezultatvp}{1}\n")
        fp.write("\\section*{{Rešitve: {0}}}".format(ime))
        for naloga in naloge:
            label = LABEL_RE.findall(naloga)[0]
            resitev = RESITEV_RE.findall(naloga)
            if len(resitev)>0:
                fp.write(RESITEV_TPL.format(label=label, resitev=resitev[0]))
            else:
                fp.write("\\addtocounter{resitevp}{1}\n")

os.system("latexmk -pdf mat_zbirka.tex")
os.system("python3 /usr/bin/pythontex mat_zbirka.tex")
os.system("latexmk -pdf mat_zbirka.tex")
